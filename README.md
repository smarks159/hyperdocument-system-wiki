# Background

This project is based on the ideas of [Doug Engelbart](https://en.wikipedia.org/wiki/Douglas_Engelbart). Doug Engelbart was one of the pioneers in computing and is responsible for inventing most of the technologies the we use related to the web and collaborative software. Engelbert realized early on that computing technologies would increase in an exponential fashion and would bring with it an exponential increase in both problems and opportunities. The problem is that human beings are not well adapted to living in an exponential world where everything is constantly changing. So we as a society must change the way we think and work in order to keep up with the changes in technology and do it quickly before the challenges brought about by technological change overwhelm our ability as a society to deal with them.

Engelbart set out to research this problem in the 1960s. He developed an organizational strategy, which he called the "Bootstrap Framework" that provides a comprehensive outline about how we need to change the way we work together to keep up with all of our new challenges. He then used this framework to invent many of the technologies that we take for granted today.

The challenges of living in an ever changing world are really becoming hard to ignore. I started this project to try and understand Engelbart's ideas about trying to solve these problems, which can be found on the [Doug Engelbart Institute website](http://www.dougengelbart.org/). Since then I have found examples of people from many different industries coming up with their own ideas to try better deal with a world of increasing change. My goal is to try and understand these ideas and apply them to solve my own problems. I feel these ideas are often as important, if not more important than the technology.

# Current Status of the System:
This software currently implements a subset of the functionality of the original NLS system, as shown in [The mother of all demos](https://www.youtube.com/watch?v=yJDv-zdhzMY). It is stable, but is missing a lot of features. I am the sole programmer and user of this system, but I use it everyday.

I write a lot, I have ideas in text files scattered all over my hard drive. Every time I start a new program project I also end up with a lot of text files describing requirements, design decisions, implementation details, todo lists, etc. I also accumlate lots of notes on observations about things I read, videos I watch etc. I find using this system to be much better than anything else I have used to write and organize my thoughts.

More details about the system are written in documents in the system itself. 

# Next version of the system
I am currently working on building the [next version of the hyperdocument system](https://gitlab.com/smarks159/hyperdocument-client-javascript-v2)

The goal of this version is to clean up the architecture and to focus on a more narrow feature set to make it more accessible to other people. It is not usable and in the design phase.

# Getting Started
## Installing The System
1. Download javascript client source code [located here](https://gitlab.com/smarks159/hyperdocument-system-js-client)
2. Download the server executable for your os [here](https://drive.google.com/folderview?id=0B0PGiHMW25e8Q2RrODktSEhabWs&usp=sharing)
3. copy the sever executable into the same directory as the client source code
4. start the server
5. to start the system open a browser and enter the address http://localhost:7000/html/TestInteractiveInterpreter.html to start the system.

## Loading the tutorial file
After starting the system next thing to do is to load the document with the tutorial that will walk you through how to use the system.

2. To load the tutorial file type "f" then "l", the system will the ask you to type in the name of a file to load. Type user_documentation into the form and hit the submit button.

3. Next to navigate to the start of the tutorial type "j" then "e" and click of the section that says "(start here)" to get started. Then follow the instructions written in the document.

# Project Code
There are two parts to this project a web based client written in javascript [located here](https://gitlab.com/smarks159/hyperdocument-system-js-client) and a server written in go [located here](https://gitlab.com/smarks159/hyperdocument-system-go-server). 

# Documentation
All of the documentation, including the user documentation are written in the system itself. A version you can view online will be implemented sometime in the future, for now you must download and install the system first to use it. See the getting started section for the instructions on how to do that.

# License
All code is licensed under the MIT license.
